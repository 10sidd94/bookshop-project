FROM centos
MAINTAINER aksarav@middlewareinventory.com
RUN mkdir /opt/tomcat/
WORKDIR /opt/tomcat
RUN curl -O https://archive.apache.org/dist/tomcat/tomcat-8/v8.5.37/bin/apache-tomcat-8.5.37.tar.gz
RUN tar xvfz apache*.tar.gz
RUN mv apache-tomcat-*/* /opt/tomcat/.
RUN yum -y install java nano
RUN java -version
WORKDIR /opt/tomcat/webapps
ADD bookshopmvn-0.0.1-SNAPSHOT.war .
COPY tomcat-users.xml /opt/tomcat/conf/
COPY context.xml /opt/tomcat/webapps/host-manager/META-INF/
COPY context.xml /opt/tomcat/webapps/manager/META-INF/
EXPOSE 8080
CMD ["/opt/tomcat/bin/catalina.sh", "run"]
