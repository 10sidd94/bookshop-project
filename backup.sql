-- MySQL dump 10.13  Distrib 5.7.36, for Linux (x86_64)
--
-- Host: localhost    Database: sunbeam_db
-- ------------------------------------------------------
-- Server version	5.7.36-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `books`
--

DROP TABLE IF EXISTS `books`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `books` (
  `id` int(4) NOT NULL,
  `name` varchar(50) DEFAULT NULL,
  `author` varchar(50) DEFAULT NULL,
  `subject` varchar(50) DEFAULT NULL,
  `price` float(7,3) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `books`
--

LOCK TABLES `books` WRITE;
/*!40000 ALTER TABLE `books` DISABLE KEYS */;
INSERT INTO `books` VALUES (1001,'Exploring C','Yashwant Kanetkar','C Programming',123.456),(1002,'Pointers in C','Yashwant Kanetkar','C Programming',371.019),(1003,'ANSI C Programming','E Balaguruswami','C Programming',334.215),(1004,'ANSI C Programming','Dennis Ritchie','C Programming',140.121),(2001,'C++ Complete Reference','Herbert Schildt','C++ Programming',417.764),(2002,'C++ Primer','Stanley Lippman','C++ Programming',620.665),(2003,'C++ Programming Language','Bjarne Stroustrup','C++ Programming',987.213),(3001,'Java Complete Reference','Herbert Schildt','Java Programming',525.121),(3002,'Core Java Volume I','Cay Horstmann','Java Programming',575.651),(3003,'Java Programming Language','James Gosling','Java Programming',458.238),(4001,'Operating System Concepts','Peter Galvin','Operating Systems',567.391),(4002,'Design of UNIX Operating System','Mauris J Bach','Operating Systems',421.938),(4003,'UNIX Internals','Uresh Vahalia','Operating Systems',352.822);
/*!40000 ALTER TABLE `books` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `mobile` varchar(15) DEFAULT NULL,
  `address` varchar(60) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `birth` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=422 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'admin','admin','1111111111','Online Book Shop','admin@onlinebooks.com','1970-01-01'),(2,'nilesh','nilesh','9527331338','Katraj, Pune','nilesh@sunbeaminfo.com','1983-09-28'),(3,'prashant','prashant','9881208114','Peth, Karad','prashant@sunbeaminfo.com','1975-12-01'),(4,'nitin','nitin','9881208115','Padmavati, Pune','nitin@sunbeaminfo.com','1974-01-23'),(20,'wrds','ree','98763423457','hjgfr','ygrfre','1970-01-01'),(22,'Pqr','pqr','8714278937','Pune','pqrs@gmail.com',NULL),(401,'Pqrt','pqr','8714278937','Pune','pqrst@gmail.com',NULL),(402,NULL,NULL,NULL,NULL,NULL,NULL),(403,NULL,NULL,NULL,NULL,NULL,NULL),(404,NULL,NULL,NULL,NULL,NULL,NULL),(405,NULL,NULL,NULL,NULL,NULL,NULL),(406,NULL,NULL,NULL,NULL,NULL,NULL),(407,NULL,NULL,NULL,NULL,NULL,NULL),(408,NULL,NULL,NULL,NULL,NULL,NULL),(409,NULL,NULL,NULL,NULL,NULL,NULL),(410,NULL,NULL,NULL,NULL,NULL,NULL),(411,NULL,NULL,NULL,NULL,NULL,NULL),(412,NULL,NULL,NULL,NULL,NULL,NULL),(413,NULL,NULL,NULL,NULL,NULL,NULL),(414,NULL,NULL,NULL,NULL,NULL,NULL),(415,'kanchan','fgdfsdf','564433344','hfrdgf','kanchan@gmail.com','2021-07-07'),(416,'PRASHANT ASHOK WAKCHAURE','$2a$10$irwtJjv/rJMTBo2R2thZ.u8wDOk2ykFZw0gXuMxch.jwUjF1pnaGW','7350588481','sangamner','prashantwakchaure2019@gmail.com','2000-05-08'),(421,'Abhiraj','$2a$10$5Joz/8990CEMUXHim0HDfuGUEowLRX4H3Gs4QaVS0.a2yGG3oSakS','34563455677','sangamner','Abhirajwakchaure2019@gmail.com','2012-02-09');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-01-05 15:09:46
