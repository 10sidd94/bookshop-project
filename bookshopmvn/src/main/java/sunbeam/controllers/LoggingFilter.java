package sunbeam.controllers;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

public class LoggingFilter implements Filter {
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
	}
	@Override
	public void destroy() {
	}
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {
		// pre-processing
		HttpServletRequest request = (HttpServletRequest) req;
		System.out.println("Requesting : " + request.getServletPath() + ", Req Param: " + request.getParameter("page"));
		// invoke service()
		chain.doFilter(req, resp);
		// post-processing
	}
}




