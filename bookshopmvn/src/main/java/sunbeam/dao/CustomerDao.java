package sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import sunbeam.pojo.Customer;
import sunbeam.util.DBUtil;

public class CustomerDao implements AutoCloseable {
	private Connection connection;
	public CustomerDao() throws Exception {
		this.connection = DBUtil.getConnection();
	}
	
	//Create / Insert
	public int insert( Customer cust )throws Exception {
		String sql = "INSERT INTO customers (name,password,mobile,address,email,birth) VALUES (?, ?, ?, ?, ?,?)";
		try(PreparedStatement insertStatement = connection.prepareStatement(sql)) { 
			insertStatement.setString(1, cust.getName());
			insertStatement.setString(2, cust.getPassword());
			insertStatement.setString(3, cust.getMobile());
			insertStatement.setString(4, cust.getAddress());
			insertStatement.setString(5, cust.getEmail());
			insertStatement.setDate(6, cust.getBirth());
			return insertStatement.executeUpdate();
		}
	}
	
	//Update
	public int update( Customer cust )throws Exception {
		String sql = "UPDATE customers SET name=?, password=?, mobile=?, address=?, email=?, birth=? WHERE id=?";
		try(PreparedStatement updateStatement = connection.prepareStatement(sql)) { 
			updateStatement.setString(1, cust.getName());
			updateStatement.setString(2, cust.getPassword());
			updateStatement.setString(3, cust.getMobile());
			updateStatement.setString(4, cust.getAddress());
			updateStatement.setString(5, cust.getEmail());
			updateStatement.setDate(6, cust.getBirth());
			updateStatement.setInt(7, cust.getId());
			return updateStatement.executeUpdate();
		}
	}

	//Delete
	public int delete( int id ) throws Exception {
		String sql = "DELETE FROM customers WHERE id=?";
		try(PreparedStatement deleteStatement = connection.prepareStatement(sql)) { 
			deleteStatement.setInt(1, id);
			return deleteStatement.executeUpdate();
		}
	}

	//Retrieve / Select
	public Customer getCustomer( int id ) throws Exception{
		String sql = "SELECT id, name, password, mobile, address, email, birth FROM customers WHERE id=?";
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) { 
			selectStatement.setInt(1, id);
			ResultSet resultSet = selectStatement.executeQuery();
			if(resultSet.next()) {
				Customer cust = new Customer(
						resultSet.getInt("id"), 
						resultSet.getString("name"),
						resultSet.getString("password"),
						resultSet.getString("email"),
						resultSet.getString("mobile"),
						resultSet.getString("address"),
						resultSet.getDate("birth"));
				return cust;
			}
		}
		return null;
	}
	
	public Customer getCustomer( String email ) throws Exception{
		String sql = "SELECT id, name, password, mobile, address, email, birth FROM customers WHERE email=?";
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) { 
			selectStatement.setString(1, email);
			ResultSet resultSet = selectStatement.executeQuery();
			if(resultSet.next()) {
				Customer cust = new Customer(
						resultSet.getInt("id"), 
						resultSet.getString("name"),
						resultSet.getString("password"),
						resultSet.getString("email"),
						resultSet.getString("mobile"),
						resultSet.getString("address"),
						resultSet.getDate("birth"));
				return cust;
			}
		}
		return null;
	}

	@Override
	public void close() {
		try {
			if(connection != null)
				connection.close();
		} catch (SQLException cause) {
			throw new RuntimeException(cause);
		}
	}
}
