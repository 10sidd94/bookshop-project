package sunbeam.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import sunbeam.pojo.Book;
import sunbeam.util.DBUtil;

public class BookDao implements AutoCloseable {
	private Connection connection;
	public BookDao() throws Exception {
		this.connection = DBUtil.getConnection();
	}
	
	//Create / Insert
	public int insert( Book book )throws Exception {
		String sql = "INSERT INTO books VALUES (?, ?, ?, ?, ?)";
		try(PreparedStatement insertStatement = connection.prepareStatement(sql)) { 
			insertStatement.setInt(1, book.getBookId());
			insertStatement.setString(2, book.getSubjectName());
			insertStatement.setString(3, book.getBookName());
			insertStatement.setString(4, book.getAuthorName());
			insertStatement.setFloat(5, book.getPrice());
			return insertStatement.executeUpdate();
		}
	}
	
	//Update
	public int update( Book book )throws Exception {
		String sql = "UPDATE books SET name = ?, author = ?, subject = ?, price = ? WHERE id = ?";
		try(PreparedStatement updateStatement = connection.prepareStatement(sql)) { 
			updateStatement.setString(1, book.getSubjectName());
			updateStatement.setString(2, book.getBookName());
			updateStatement.setString(3, book.getAuthorName());
			updateStatement.setFloat(4, book.getPrice());
			updateStatement.setInt(5, book.getBookId());
			return updateStatement.executeUpdate();
		}
	}

	//Delete
	public int delete( int id )throws Exception {
		String sql = "DELETE FROM books WHERE id = ?";
		try(PreparedStatement updateStatement = connection.prepareStatement(sql)) { 
			updateStatement.setInt(1, id);
			return updateStatement.executeUpdate();
		}
	}

	//Retrieve / Select
	public List<Book> getBooks( ) throws Exception{
		String sql = "SELECT id, name, author, subject, price FROM books";
		List<Book> books = new ArrayList<Book>();
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) {
			try (ResultSet rs = selectStatement.executeQuery()) {
				while (rs.next())
					books.add(new Book(rs.getInt("id"), rs.getString("subject"), rs.getString("name"),  rs.getString("author"), rs.getFloat("price")));
			}
		}
		return books;
	}
	
	public Book getBook( int id ) throws Exception{
		String sql = "SELECT id, name, author, subject, price FROM books WHERE id = ?";
		Book book = null;
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) {
			selectStatement.setInt(1, id);
			try (ResultSet rs = selectStatement.executeQuery()) {
				if (rs.next())
					book = new Book(rs.getInt("id"), rs.getString("subject"), rs.getString("name"),  rs.getString("author"), rs.getFloat("price"));
			}
		}
		return book;
	}

	@Override
	public void close() {
		try {
			if(connection != null)
				connection.close();
		} catch (SQLException cause) {
			throw new RuntimeException(cause);
		}
	}
	
	public List<String> getSubejcts() throws Exception {
		String sql = "SELECT DISTINCT subject FROM books";
		List<String> subjects = new ArrayList<>();
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) {
			try (ResultSet rs = selectStatement.executeQuery()) {
				while (rs.next())
					subjects.add(rs.getString("subject"));
			}
		}
		return subjects;
	}
	
	public List<Book> getBooks(String subject)  throws Exception {
		String sql = "SELECT id, name, author, subject, price FROM books WHERE subject = ?";
		List<Book> books = new ArrayList<Book>();
		try(PreparedStatement selectStatement = connection.prepareStatement(sql)) {
			selectStatement.setString(1, subject);
			try (ResultSet rs = selectStatement.executeQuery()) {
				while (rs.next())
					books.add(new Book(rs.getInt("id"), rs.getString("subject"), rs.getString("name"),  rs.getString("author"), rs.getFloat("price")));
			}
		}
		return books;
	}
}
