package sunbeam.beans;

import sunbeam.dao.CustomerDao;
import sunbeam.pojo.Customer;

public class LoginBean {
	private String email;
	private String password;
	private boolean success;
	
	public LoginBean() {
		this.email = "";
		this.password = "";
		this.success = false;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public void authenticate() {
		try (CustomerDao dao = new CustomerDao()) {
			Customer cust = dao.getCustomer(this.email);
			success = cust.getPassword().equals(this.password);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}








