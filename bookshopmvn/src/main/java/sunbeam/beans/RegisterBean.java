package sunbeam.beans;

import java.sql.Date;
import java.text.SimpleDateFormat;

import sunbeam.dao.CustomerDao;
import sunbeam.pojo.Customer;

public class RegisterBean {
	private int id;
	private String name;
	private String password;
	private String email;
	private String mobile;
	private String address;
	private String birth;
	private boolean success;

	public RegisterBean() {
		this.id = 0;
		this.name = "";
		this.password = "";
		this.email = "";
		this.mobile = "";
		this.address = "";
		this.birth = "";
		this.success = false;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getBirth() {
		return birth;
	}

	public void setBirth(String birth) {
		this.birth = birth;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public void add() {
		try (CustomerDao dao = new CustomerDao()) {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date birth = new Date(sdf.parse(this.birth).getTime());
			Customer c = new Customer(this.id, this.name, this.password, this.email, this.mobile, this.address, birth);
			int count = dao.insert(c);
			this.success = (count == 1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
