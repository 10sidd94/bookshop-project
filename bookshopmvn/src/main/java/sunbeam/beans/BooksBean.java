package sunbeam.beans;

import java.util.ArrayList;
import java.util.List;

import sunbeam.dao.BookDao;
import sunbeam.pojo.Book;

public class BooksBean {
	private String subject;
	private List<Book> bookList;
	
	public BooksBean() {
		this.subject = "";
		this.bookList = new ArrayList<Book>();
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public List<Book> getBookList() {
		return bookList;
	}

	public void setBookList(List<Book> bookList) {
		this.bookList = bookList;
	}
	
	public void fetchBooks() {
		try (BookDao dao = new BookDao()) {
			this.bookList = dao.getBooks(this.subject);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}




