package sunbeam.beans;

import sunbeam.dao.BookDao;
import sunbeam.pojo.Book;

public class DetailsBean {
	private int id;
	private String name;
	private String author;
	private String subject;
	private float price;
	private boolean success;
	
	public DetailsBean() {
		this.id = 0;
		this.name = "";
		this.author = "";
		this.subject = "";
		this.price = 0.0f;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}
	
	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public void find() {
		try(BookDao dao = new BookDao()) {
			Book b = dao.getBook(this.id);
			this.success = (b != null);
			if(this.success) {
				this.name = b.getBookName();
				this.author = b.getAuthorName();
				this.subject = b.getSubjectName();
				this.price = b.getPrice();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	// to be called from admin part
	public void add() {
		// TODO: Lab assignment
	}
	
	// to be called from admin part
	public void update() {
		// TODO: Lab assignment
	}
	
	// to be called from admin part
	public void delete() {
		// TODO: Lab assignment
	}
}
