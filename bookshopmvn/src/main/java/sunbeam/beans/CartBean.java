package sunbeam.beans;

import java.util.ArrayList;
import java.util.List;

public class CartBean {
	private String[] bookIds;
	private List<Integer> cart;
	public CartBean() {
		this.bookIds = new String[0];
		this.cart = new ArrayList<Integer>();
	}
	public String[] getBookIds() {
		return bookIds;
	}
	public void setBookIds(String[] bookIds) {
		this.bookIds = bookIds;
	}
	public List<Integer> getCart() {
		return cart;
	}
	public void setCart(List<Integer> cart) {
		this.cart = cart;
	}
	public void addCart() {
		for (int i = 0; i < bookIds.length; i++) {
			int id = Integer.parseInt(bookIds[i]);
			if(!cart.contains(id))
				cart.add(id);
		}
		//System.out.println(cart);
	}
}









