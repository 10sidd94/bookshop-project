<%@page import="sunbeam.beans.LoginBean"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Auth</title>
</head>
<body>
	<jsp:useBean id="lb" class="sunbeam.beans.LoginBean"/>
	<jsp:setProperty name="lb" property="*" />
	${ lb.authenticate() }
	
	<c:choose>
		<c:when test="${lb.success = true}">
			<c:redirect url="ctl?page=subjects"/>
		</c:when>
		<c:otherwise>
			Invalid email or password. <br/>
			<a href="ctl?page=login">Login again</a>
		</c:otherwise>
	</c:choose>
</body>
</html>







