<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
</head>
<body>
	<jsp:useBean id="bb" class="sunbeam.beans.BooksBean"/>
	<jsp:setProperty name="bb" property="*" />
	${bb.fetchBooks()}
	<form method="post" action="ctl?page=addcart">
		<c:forEach var="b" items="${bb.bookList}">
			<input type="checkbox" name="book" value="${b.bookId}"/> ${b.bookName} <br/>
		</c:forEach>
		<input type="submit" value="Add Cart"/>
	</form>
</body>
</html>




