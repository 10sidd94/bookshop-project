<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Thanks</title>
</head>
<body>
	<% session.invalidate(); %>
	Thank you for using our services.
	<br/><br/>
	<a href="ctl?page=login">Login Again</a>
</body>
</html>

