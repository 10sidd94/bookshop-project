<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title></title>
</head>
<body>
	<jsp:useBean id="cb" class="sunbeam.beans.CartBean" scope="session"/>
	<jsp:setProperty name="cb" property="bookIds" param="book" />
	${cb.addCart()}
	<jsp:forward page="ctl?page=subjects"/>
</body>
</html>
