<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration</title>
</head>
<body>
	<jsp:useBean id="rb" class="sunbeam.beans.RegisterBean"/>
	<jsp:setProperty name="rb" property="*" />
	${rb.add()}
	<c:choose>
		<c:when test="${rb.success == true}">
			<c:redirect url="ctl?page=login"/>		
		</c:when>
		<c:otherwise>
			Registration failed. <br/>
			<a href="ctl?page=register">Try again</a>
		</c:otherwise>
	</c:choose>
</body>
</html>




