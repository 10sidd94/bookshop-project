<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Cart</title>
</head>
<body>
	<jsp:useBean id="db" class="sunbeam.beans.DetailsBean"/>
	<jsp:useBean id="cb" class="sunbeam.beans.CartBean" scope="session"/>

	<c:set var="total" value="0.0"/>
	<table border="1">
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Author</td>
				<td>Subject</td>
				<td>Price</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="id" items="${cb.cart}">
				<jsp:setProperty name="db" property="id" value="${id}"/>
				${db.find()} <%-- not checking success for sake of simplicity --%>
				<tr>
					<td>${db.id}</td>
					<td>${db.name}</td>
					<td>${db.author}</td>
					<td>${db.subject}</td>
					<td>${db.price}</td>
				</tr>			
				<c:set var="total" value="${total + db.price}"/>
			</c:forEach>
		</tbody>
	</table>
	<br/>
	Total Price: Rs. <fmt:formatNumber value="${total}" type="number" minFractionDigits="2" maxFractionDigits="2" /> /-
	<br/><br/>
	<a href="ctl?page=logout">Sign Out</a>
</body>
</html>


